//
//  NeuralNetworkTests.swift
//  NeuralNetworkTests
//
//  Created by Jasper Siebelink on 13/04/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import XCTest
@testable import NeuralNetwork

final class NeuralNetworkTests: XCTestCase {
    
    func testOutputNeuronTraining() {
        // ARRANGE
        let sut: OutputNeuron = OutputNeuron(givenBias: 0.3)
        sut.expectedOutput = 0
        
        // ACT
        sut.appendInput(0.5)
        sut.determineNewBias()
        sut.applyDeterminedNetworkUpdates()
        
        // ASSERT
        XCTAssertEqual(sut.bias,
                       0.3625)
    }
    
    func testHiddenNeuronTraining() {
        // ARRANGE
        let sut: Neuron = Neuron(outputWeightsCount: 1,
                                 givenWeights: [-1.2],
                                 givenBias: 0.8)
        
        // ACT
        sut.appendInput(0.5)
        sut.determineWeightCorrection(outputNeuronErrorGradient: 0.05,
                                      outputPosition: 0)
        sut.determineNewBias(errorGradient: 0.05)
        
        sut.applyDeterminedNetworkUpdates()

        // ASSERT
        XCTAssertEqual(sut.outputWeights[0],
                       -1.1875)
        XCTAssertEqual(sut.bias,
                       0.775)
    }
    
    func testSingleIteration() {
        // ARRANGE
        let sut: NeuralNetwork = NeuralNetwork(numInputsNeurons: 2,
                                               numHiddenNeurons: 2,
                                               numOutputNeurons: 1,
                                               fixedInputWeights: [[0.5, 0.9],
                                                                   [0.4, 1.0]],
                                               fixedHiddenWeights: [[-1.2],
                                                                    [1.1]],
                                               fixedHiddenBias: [0.8, -0.1],
                                               fixedOutputBias: [0.3])
        
        // ACT
        let sumSquaredError: CGFloat = sut.trainIteration(input: [1.0, 1.0],
                                                          expectedResult: [0.0])
        
        // ASSERT sum of squared error
        XCTAssertEqual(sumSquaredError,
                       0.25981877422443106)
        
        // ASSERT weights have been modified
        XCTAssertEqual(sut.configuredWeights,
                       [[[0.5190597389983806, 0.8926440896045258], [0.4190597389983806, 0.9926440896045258]],  // Input layer
                        [[-1.2334366735379192], [1.0439007773033266]]]) // Hidden layer
        
        // ASSERT bias has been modified
        XCTAssertEqual(sut.configuredBiases,
                       [[0.7809402610016194, -0.09264408960452573], // Hidden layer
                        [0.3636914268896815]]) // Output layer
    }
    
    func testSameNetworkApplicationHasSameOutcome() {
        // ARRANGE
        let sut: NeuralNetwork = NeuralNetwork(numInputsNeurons: 1,
                                               numHiddenNeurons: 1,
                                               numOutputNeurons: 1)
        
        // ACT
        let firstOutput = sut.applyNetwork(values: [0.5])
        let secondOutput = sut.applyNetwork(values: [0.5])
        
        // ASSERT
        XCTAssertEqual(firstOutput,
                       secondOutput)
    }
    
    func testTrainXOR() {
        // ARRANGE
        let sut: NeuralNetwork = NeuralNetwork(numInputsNeurons: 2,
                                               numHiddenNeurons: 2,
                                               numOutputNeurons: 1,
                                               fixedInputWeights: [[0.5, 0.9],
                                                                   [0.4, 1.0]],
                                               fixedHiddenWeights: [[-1.2],
                                                                    [1.1]],
                                               fixedHiddenBias: [0.8, -0.1],
                                               fixedOutputBias: [0.3])
        
        // ACT: Train the network
        var sumSquaredError: CGFloat = 0
        repeat {
            sumSquaredError = [([1.0, 1.0], [0.0]),
                               ([0.0, 1.0], [1.0]),
                               ([1.0, 0.0], [1.0]),
                               ([0.0, 0.0], [0.0])].reduce(0, { (result, tuple) -> CGFloat in
                                return result + sut.trainIteration(input: tuple.0,
                                                                   expectedResult: tuple.1)
                               })
        } while (sumSquaredError/4) > 0.001
        
        // ASSERT
        let validationEntries: [([CGFloat], Bool)] = [([1.0, 1.0], false),
                                                     ([0.0, 1.0], true),
                                                     ([1.0, 0.0], true),
                                                     ([0.0, 0.0], false)]
        validationEntries.forEach { (validationEntry) in
            let networkOutput: Bool = Int(round(sut.applyNetwork(values: validationEntry.0)[0])) == 1
            let desiredOutput: Bool = validationEntry.1
            XCTAssertEqual(networkOutput,
                           desiredOutput)
        }
    }
}
