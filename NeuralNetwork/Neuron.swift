//
//  Neuron.swift
//  NeuralNetwork
//
//  Created by Jasper Siebelink on 13/04/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

class Neuron {
    struct Constants {
        static let randomBiasRange: CGFloat = 2
        static let randomWeightRange: CGFloat = 4
        static let learningRate: CGFloat = 0.5
    }
    
    // Current configuration
    private(set) var currentOutput: CGFloat = 0
    var outputWeights: [CGFloat] = []
    var bias: CGFloat = 0
    
    // Training data
    var newWeights: [CGFloat] = []
    var biasDelta: CGFloat = 0
    
    init(outputWeightsCount: Int = 0,
         givenWeights: [CGFloat]? = nil,
         givenBias: CGFloat? = nil)
    {
        outputWeights = givenWeights ?? Array(0..<outputWeightsCount).map { (_) -> CGFloat in
            return Neuron.randomValue(Constants.randomWeightRange)
        }
        
        bias = givenBias ?? Neuron.randomValue(Constants.randomBiasRange)
        
        resetCache()
    }
    
    // Return a non-0 value of (range/2) above or below 0
    private static func randomValue(_ range: CGFloat) -> CGFloat {
        var randomValue: CGFloat = 0
        repeat {
            randomValue = (CGFloat(arc4random()%UInt32(range)*10)/10) - (range/2)
        } while randomValue == 0.0
        return randomValue
    }
    
    // Used for summing the input
    func appendInput(_ value: CGFloat) {
        currentOutput += value
    }
    
    // Apply the bias and activation function on top of the summed input
    func determineOutput() {
        currentOutput = sigmoidActivationFunction(value: currentOutput + (-1 * bias))
    }
    
    private func sigmoidActivationFunction(value: CGFloat) -> CGFloat {
        return (1.0 / (1.0 + exp(-value)))
    }
    
    // MARK: Training
    func determineNewBias(errorGradient: CGFloat) {
        let biasCorrection: CGFloat = Constants.learningRate * (-1) * errorGradient
        biasDelta += biasCorrection
    }
    
    func determineWeightCorrection(outputNeuronErrorGradient: CGFloat,
                                   outputPosition: Int) {
        // Correct the weight
        let weightCorrection: CGFloat = Constants.learningRate * currentOutput * outputNeuronErrorGradient
        newWeights[outputPosition] = newWeights[outputPosition] + outputWeights[outputPosition] + weightCorrection
    }
    
    func applyDeterminedNetworkUpdates() {
        outputWeights = newWeights
        bias += (biasDelta / CGFloat(outputWeights.count))
        resetCache()
    }
    
    func resetCache() {
        currentOutput = 0

        newWeights = Array(repeating: 0,
                           count: outputWeights.count)
        biasDelta = 0
    }
}
