//
//  NeuralNetwork.swift
//  NeuralNetwork
//
//  Created by Jasper Siebelink on 13/04/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// Feed-forward back-propagation network
final class NeuralNetwork {
    
    typealias NeuronLayer = [Neuron]
    
    fileprivate let inputNeurons: NeuronLayer
    fileprivate let hiddenNeurons: NeuronLayer
    fileprivate let outputNeurons: [OutputNeuron]
    
    // Create an acyclic feed-forward system
    init(numInputsNeurons: Int,
         numHiddenNeurons: Int,
         numOutputNeurons: Int,
         fixedInputWeights: [[CGFloat]]? = nil,
         fixedHiddenWeights: [[CGFloat]]? = nil,
         fixedHiddenBias: [CGFloat]? = nil,
         fixedOutputBias: [CGFloat]? = nil)
    {
        inputNeurons = Array(0..<numInputsNeurons).map({ (position) -> Neuron in
            return Neuron(outputWeightsCount: numHiddenNeurons,
                          givenWeights: fixedInputWeights?[safe: position],
                          givenBias: 0)
        })
        
        hiddenNeurons = Array(0..<numHiddenNeurons).map({ (position) -> Neuron in
            return Neuron(outputWeightsCount: numOutputNeurons,
                          givenWeights: fixedHiddenWeights?[safe: position],
                          givenBias: fixedHiddenBias?[safe: position])
        })
        
        outputNeurons = Array(0..<numOutputNeurons).map({ (position) -> OutputNeuron in
            return OutputNeuron(givenBias: fixedOutputBias?[safe: position])
        })
    }
    
    @discardableResult
    func applyNetwork(values: [CGFloat],
                      resetAfterCompletion: Bool = true) -> [CGFloat] {
        guard values.count == inputNeurons.count else {
            print("Invalid input count supplied: \(values.count), expected: \(inputNeurons.count)")
            return Array(repeating: 0,
                         count: outputNeurons.count)
        }

        // Input neurons in inputlayer
        for (valuePos, value) in values.enumerated() {
            let inputNeuron: Neuron = inputNeurons[valuePos]
            inputNeuron.appendInput(value)
        }
        
        // Determine the hidden layer values
        applyInput(currentLayer: inputNeurons,
                   nextLayer: hiddenNeurons)
        
        // Determine the output layer values
        applyInput(currentLayer: hiddenNeurons,
                   nextLayer: outputNeurons)
        
        let output: [CGFloat] = outputNeurons.map({ return $0.currentOutput })
        
        if resetAfterCompletion {
            (inputNeurons + hiddenNeurons + outputNeurons).forEach({ $0.resetCache() })
        }
        
        return output
    }
    
    private func applyInput(currentLayer: NeuronLayer,
                            nextLayer: NeuronLayer) {
        currentLayer.forEach { (inputNeuron) in
            let inputNeuronValue: CGFloat = inputNeuron.currentOutput
            
            // Iterate over each hidden-layer neuron and apply the value
            for (hiddenNeuronPos, hiddenNeuron) in nextLayer.enumerated() {
                let weightedValue: CGFloat = inputNeuronValue * inputNeuron.outputWeights[hiddenNeuronPos]
                hiddenNeuron.appendInput(weightedValue)
            }
        }
        nextLayer.forEach({ $0.determineOutput() })
    }
    
    // MARK: Retrieve the current network configuration values
    func printWeightsAndBias() {
        print()
        print("  Input weights:")
        inputNeurons.forEach({ print("    \($0.outputWeights)") })
        
        print("  Hidden layer weights:")
        hiddenNeurons.forEach({ print("    \($0.outputWeights)") })
        
        print("  Hidden layer bias:")
        hiddenNeurons.forEach({ print("    \($0.bias)") })
        
        print("  Output layer bias:")
        outputNeurons.forEach({ print("    \($0.bias)") })
        print()
    }
    
    var configuredBiases: [[CGFloat]] {
        return [hiddenNeurons.map({ return $0.bias }),
                outputNeurons.map({ return $0.bias })]
    }
    
    var configuredWeights: [[[CGFloat]]] {
        return [inputNeurons.map({ return $0.outputWeights }),
                hiddenNeurons.map({ return $0.outputWeights })]
    }
}

// MARK: Training
extension NeuralNetwork {
    // Returns true when the loss is sufficiently low
    @discardableResult
    func trainIteration(input: [CGFloat],
                        expectedResult: [CGFloat]) -> CGFloat {
        // Apply the inputs
        applyNetwork(values: input,
                     resetAfterCompletion: false)
        
        // Preset expected values
        for (outputPos, expectedOutputValue) in expectedResult.enumerated() {
            outputNeurons[outputPos].expectedOutput = expectedOutputValue
        }
        
        // Determine total error
        let totalSquaredError: CGFloat = pow(outputNeurons.reduce(0) { (result, outputNeuron) -> CGFloat in
            return result + outputNeuron.error
        }, 2)
        
        // Determine the output neuron biases
        outputNeurons.forEach({ $0.determineNewBias() })
    
        // Determine the hidden neuron weights and biases
        var hiddenNeuronErrorGradients: [[CGFloat]] = Array(repeating: [],
                                                            count: hiddenNeurons.count)
        for (hiddenNeuronPos, hiddenNeuron) in hiddenNeurons.enumerated() {
            for (outputNeuronPos, outputNeuron) in outputNeurons.enumerated() {
                hiddenNeuron.determineWeightCorrection(outputNeuronErrorGradient: outputNeuron.outputErrorGradient,
                                                       outputPosition: outputNeuronPos)
                
                // Precalculate neuron's error gradient for its inputs
                let errorGradientForWeight: CGFloat = hiddenNeuron.currentOutput * (1 - hiddenNeuron.currentOutput) * outputNeuron.outputErrorGradient * hiddenNeuron.outputWeights[outputNeuronPos]
                hiddenNeuronErrorGradients[hiddenNeuronPos].append(errorGradientForWeight)
                
                // For each output neuron, determine the bias correction
                hiddenNeuron.determineNewBias(errorGradient: errorGradientForWeight)
            }
        }
        
        // Determine the input neurons weights
        for outputNeuronPos in 0..<outputNeurons.count {
            for hiddenNeuronPos in 0..<hiddenNeurons.count {
                for inputNeuron in inputNeurons {
                    let errorGradient: CGFloat = hiddenNeuronErrorGradients[hiddenNeuronPos][outputNeuronPos]
                    inputNeuron.determineWeightCorrection(outputNeuronErrorGradient: errorGradient,
                                                          outputPosition: hiddenNeuronPos)
                }
            }
        }

        // Apply all new weights and biases
        (inputNeurons + hiddenNeurons + outputNeurons).forEach({ (neuron) in
            neuron.applyDeterminedNetworkUpdates()
        })
        
        return totalSquaredError
    }
}

private extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
