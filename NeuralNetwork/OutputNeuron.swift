//
//  OutputNeuron.swift
//  NeuralNetwork
//
//  Created by Jasper Siebelink on 14/04/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

final class OutputNeuron: Neuron {
    
    var expectedOutput: CGFloat = 0
    
    var outputErrorGradient: CGFloat {
        let calculatedErrorGradient: CGFloat = currentOutput * (1 - currentOutput) * error
        return calculatedErrorGradient
    }
    
    func determineNewBias() {
        let biasCorrection: CGFloat = Constants.learningRate * (-1) * outputErrorGradient
        biasDelta = biasCorrection
    }
    
    override func applyDeterminedNetworkUpdates() {
        bias = bias + biasDelta
        resetCache()
    }
    
    var error: CGFloat {
        return expectedOutput - currentOutput
    }
}
